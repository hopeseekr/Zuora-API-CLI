<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use PHPExperts\ZuoraClient\DTOs\Write\ContactDTO;
use PHPExperts\ZuoraClient\ZuoraClient;

class ViewAccountInvoices extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'account:invoices:list {--full} {zuoraId}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Lists a Zuora Customer Account\'s invoices.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var ZuoraClient $zuora */
        $zuora = app('zuora');

        $invoices = $zuora->account
            ->id($this->argument('zuoraId'))
            ->invoice->fetchSummary();

        if ($this->option('full')) {
            dd($invoices);
        }

        $payload = [];
        foreach ($invoices as $index => $invoice) {
            $payload[] = [
                'Num'            => $index + 1,
                'Account ID'     => $invoice->accountId,
                'Name'           => $invoice->accountName,
                'Status'         => $invoice->status,
                'Invoice Date'   => $invoice->invoiceDate->toDateString(),
                'Amount'         => $invoice->amount,
                'Balance'        => $invoice->balance,
                'Credit Balance' => $invoice->creditBalance,
            ];
        }

        if (empty($payload)) {
            $this->line('No invoices were found.');

            return;
        }

        $this->table(array_keys($payload[0]), $payload);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}

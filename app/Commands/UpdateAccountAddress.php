<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use PHPExperts\ZuoraClient\DTOs\ContactDTO;
use PHPExperts\ZuoraClient\ZuoraClient;

class UpdateAccountAddress extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'account:address:update {zuoraId}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Updates a Zuora Customer Account\'s Bill-To and/or Sold-To addresses.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contactDTO = new ContactDTO(['city', 'State']);

        /** @var ZuoraClient $zuora */
        $zuora = app('zuora');
        $info = $zuora->account->fetch($this->argument('zuoraId'));

        //$zuora->contact->update()
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}

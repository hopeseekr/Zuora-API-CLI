<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use PHPExperts\ZuoraClient\DTOs\ContactDTO;
use PHPExperts\ZuoraClient\Exceptions\ZuoraAPIException;
use PHPExperts\ZuoraClient\ZuoraClient;
use RuntimeException;

class DeleteAccounts extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'accounts:delete {zuoraIds}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Deletes one or more Zuora Customer Accounts.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $zuoraIds = explode(',', $this->argument('zuoraIds'));

        /** @var ZuoraClient $zuora */
        $zuora = app('zuora');

        foreach ($zuoraIds as $zuoraId) {
            $this->line("Deleting account: $zuoraId");
            $response = $zuora->account->id($zuoraId)->destroy();
            if ($response !== true) {
                throw new ZuoraAPIException("Couldn't delete Zuora ID $zuoraId");
            }
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}

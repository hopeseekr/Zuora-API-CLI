<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use PHPExperts\ZuoraClient\ZuoraClient;

class ViewAccount extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'account:view {zuoraId}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Displays the details of a Zuora Customer Account.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var ZuoraClient $zuora */
        $zuora = app('zuora');
        $info = $zuora->account->id($this->argument('zuoraId'))->fetch();
        dd($info->toArray());
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}

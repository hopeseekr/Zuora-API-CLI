<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use PHPExperts\ZuoraClient\ZuoraClient;

class FindAccounts extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'accounts:find {name}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Displays info of one or more Zuora Customer Accounts.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        /** @var ZuoraClient $zuora */
        $zuora = app('zuora');
        $accounts = $zuora->account->findByName($name);

        dump($accounts);

        $zuoraIds = implode(',', array_column($accounts, 'Id'));
        dd($zuoraIds);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}

<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use PHPExperts\ZuoraClient\DTOs\Write\AccountDTO;
use PHPExperts\ZuoraClient\DTOs\Write\ContactDTO;
use PHPExperts\ZuoraClient\DTOs\Response;
use PHPExperts\ZuoraClient\ZuoraClient;

class CreateAccount extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'account:create {name} {country} {currency}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Creates a new Zuora Customer Account.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var ZuoraClient $zuora */
        $zuora = app('zuora');

        $name = $this->argument('name');
        $country = $this->argument('country');
        $currency = $this->argument('currency');

        $billToContactDTO = new ContactDTO();
        $billToContactDTO->firstName = explode(' ', $name)[0];
        $billToContactDTO->lastName = explode(' ', $name)[1];
        $billToContactDTO->country = "US";

        $accountDTO = new AccountDTO();
        $accountDTO->name = $name;
        $accountDTO->currency = $currency;
        $accountDTO->billToContact = $billToContactDTO;
        $accountDTO->billCycleDay = 15;

        try {
            /** @var Response\AccountCreatedDTO $accountCreatedDTO */
            $accountCreatedDTO = $zuora->account->store($accountDTO);
            if ($accountCreatedDTO->success === false) {
                throw \Error("Couldn't create an account because of an umknown failure.");
            }
        } catch (\Throwable $e) {
            $this->error("Couldn't create an account because: " . $e->getMessage(), 1);
            exit;
        }

        $accountId = $accountCreatedDTO->accountId;
        $accountNumber = $accountCreatedDTO->accountNumber;
        $this->line("Account created successfully!");
        $this->line(" - AccountID (internal): $accountId");
        $this->line(" - Account Number (external): $accountNumber");
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}

<?php

namespace Tests\Feature;

use PHPExperts\ZuoraClient\ZuoraClient;
use Tests\TestCase;

class InvoicesTest extends TestCase
{
    public function testCanViewInvoices()
    {
        // @FIXME: We need to generate an invoice via tests instead of relying on existing ZuoraIDs.
        $zuoraId = '';
        if (!$zuoraId) {
            throw new \LogicException('Need to put in place a working ZuoraID.');
        }

        /** @var ZuoraClient $zuora */
        $zuora = app('zuora');
        $info = $zuora->account->id($zuoraId)->fetch();
    }
}
